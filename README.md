# SuLo

Veebipõhine suurkiskjate vaatlusandmete edastamine

Suurkiskjad -- karu (Ursus arctos), hunt (Canis lupus) ja ilves (Lynx lynx) on rahvusvahelise tähtsusega liigid ning Euroopas rangelt kaitstavad. Eestis on suurkiskjate asurkonnad ühed Euroopa tugevamad ja elujõulisemad, mistõttu on lubatud siin nende loomade reguleeritud küttimine. Suurkiskjate asurkondade kaitse ja optimaalse kasutamise aluseks on aga järjepidev seire.
Praegu täidetakse suurkiskjate vaatluslehed ja ankeedid arvutis või paberkandjal, skanneritud ankeedid edastatakse Keskkonnaagentuuri eluslooduseosakonna e-posti aadressile ulukiseire@envir.ee

**Eesmärgiks lihtsustada vaatlusandmete edastamist ja muuta info operatiivsemaks, luues esialgselt veebirakendus, mida on võimalik teises faasis ümber muuta äpiks.**
Sihtgrupiks eluslooduse andmeid koguvad inimesed - keskkonnaamet, teadlased, jahimehed, loodushuvilised.

**Mockup**
https://gomockingbird.com/projects/vh4jknj/mLpv2G

User story:
* [ ] - Kasutaja siseneb veebi / loob konto
* [ ] - Kasutaja loob ja salvestab oma profiili andmetega:
* [ ] -       Nimi* kohustuslik
* [ ] -       Kontakt nr* kohustuslik
* [ ] -  Kasutaja sisestab vaatlusandmed: ILVES, HUNT, KARU
* [ ] -  Kasutaja sisestab mida ta näeb: ISEND, JÄLG;
* [ ] -  HUNT ja ILVES sama meetod sisestamiseks:
* [ ] -           ISend   - kas täiskasvanud ja mitu (inT)
* [ ] -                   - kas kutsikas ja mitu (int)
* [ ] -                   -edasi suunatakse GPS lehele, kus ka kommentaar
* [ ] -           Jälg (edasi GPS lehele suunamine, kus ka kommentaar)
* [ ] -  KARU siis teine meetod:
* [ ] -           Isend   - mitu täiskasvanut ja kui suur esikäpa laius, call 112!
* [ ] -                   - mitu poega alla 1 a, mitu poega üle 1 a, esikäpa lais
* [ ] -           Jälg  (esikäpa laius)
* [ ] -                   - edasi GPS lehele suunamine, kus kommentaar.
* [ ] -  GPS+pilt+kommentara lehele
* [ ] -           - GPS/GNSS - pärib geolokatsiooni äpist?? :)
* [ ] -           - Pildi üleslaadimine päris telefonist
* [ ] -           - Kommentaar (string kuni 200tm)
* [ ] -           - edastab (KUIDAS??? meiliga? serverisse?)
* [ ] -  Täname lehekülg
* [ ] -           - Kas ka kuvame edastatud andmete tabelit?


