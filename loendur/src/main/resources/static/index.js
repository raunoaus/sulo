console.log("Loendame kiskjaid")
var laiusKraad
var pikkusKraad
var nahtudHunt
var nahtudIlves
var nahtudKaru

var insertData = async function() {
    var APIurl = location.origin + '/suki'
    var request = await fetch(APIurl)
    var data = await request.json()
}
document.querySelector('form').onsubmit=(e)=>e.preventDefault()

document.querySelector('#final-btn').onclick = async function(event) {
    event.preventDefault()
    document.getElementById('Tanan').classList.remove("hidden")
    document.getElementById('Edasta').classList.add("hidden")


    var id = Math.random().toString(36).substring(2, 4) + Math.random().toString(36).substring(2, 4);
    console.log("random id ", id)
    var nimi = document.querySelector('#nimi').value
    var telefon = document.querySelector('#telefon').value
    var nrsuurHunt = document.querySelector('#nrsuurHunt').value
    var nrkutsikasHunt = document.querySelector('#kutsHNr').value
    var hundiJalg = nahtudHunt
    console.log("Hunt: ", nahtudHunt)
    var nrsuurIlves = document.querySelector('#ilvesNr').value
    var nrkutsikasIlves = document.querySelector('#kutsINr').value
    var ilvesJalg = nahtudIlves
    console.log("Ilves: ", nahtudIlves)
    var nrvanemKaru = document.querySelector('#vanemadKarud').value
    var pakacmVanem = document.querySelector('#pakaSuuri').value
    var nrBeebiKaru = document.querySelector('#allaykspojad').value
    var nrNoorKaru = document.querySelector('#vanemadpojad').value
    var pakacmNoorem = document.querySelector('#pakaNoorem').value
    var nahtudKaruJalg = nahtudKaru
    var pakacmKaru = document.querySelector('#pakaSuurj').value
    var laiuskraad = laiusKraad
    console.log("Laiuskraad: ", laiusKraad)
    var pikkuskraad = pikkusKraad
    console.log("Pikkuskraad: ", pikkusKraad)
    var kommentaar = document.querySelector('#kommentaar').value

    var sisu = {
       id: id,
       nimi: nimi,
       telefon: telefon,
       huntide_arv: parseInt(nrsuurHunt),
       hundikutsikate_arv: parseInt(nrkutsikasHunt),
       nahtud_hundi_jalg: hundiJalg,
       ilveste_arv: parseInt(nrsuurIlves),
       ilvese_kutsikate_arv: parseInt(nrkutsikasIlves),
       nahtud_ilvese_jalg: ilvesJalg,
       karude_arv: parseInt(nrvanemKaru),
       karu_paka_cm: parseFloat(pakacmVanem),
       karupoegi_kuni_aastased: parseInt(nrBeebiKaru),
       karupoegi_yle_aasta: parseInt(nrNoorKaru),
       karupoja_paka_cm: parseFloat(pakacmNoorem),
       nahtud_karu_jalg: nahtudKaru,
       karu_jalg_paka_cm: parseFloat(pakacmKaru),
       laiuskraad: laiusKraad,
       pikkuskraad: pikkusKraad,
       kommentaar: kommentaar
   }

   var APIurl = location.origin + '/suki'
   fetch(APIurl, {
      method: "POST",
      body: JSON.stringify(sisu),
      headers:{
          'Accept': 'application/json',
          'Content-Type': 'application/json'
      }
  })
  console.log("Sisu: ", sisu)
}

document.getElementById('avaleht-btn').onclick= function (event){
    document.getElementById('Valik').classList.remove("hidden")
    document.getElementById('Avaleht').classList.add("hidden")
}

document.getElementById('Tagasi1-btn').onclick= function (event){
    document.getElementById('Avaleht').classList.remove("hidden")
    document.getElementById('Valik').classList.add("hidden")
    }

var track
document.querySelectorAll('#Isend, #Jalg').forEach(function(item){
    item.onclick = function (event){
    track = event.target.id
    document.getElementById('Kiskjad').classList.remove("hidden")
    document.getElementById('Valik').classList.add("hidden")
}
})


document.getElementById('Tagasi2-btn').onclick= function (event){
    document.getElementById('Valik').classList.remove("hidden")
    document.getElementById('Kiskjad').classList.add("hidden")
    }


var karu
var ilves
var hunt
document.getElementById('Kiskjad').onclick= function (event){
    karu = event.target.id
    ilves = event.target.id
    hunt = event.target.id
    console.log("karu:", karu)
    console.log("track:", track)
    if  (track === 'Jalg' && karu === 'Karu-btn' ){
         nahtudKaru = "Nähtud karu jälgi"
         document.getElementById('Karujalg').classList.remove("hidden")
         document.getElementById('Kiskjad').classList.add("hidden")
         return nahtudKaru
    }else if (track==='Jalg' && ilves==='Ilves-btn'){
         nahtudIlves = "Nähtud ilvese jälgi"
         document.getElementById('Edasta').classList.remove("hidden")
         document.getElementById('Kiskjad').classList.add("hidden")
         return nahtudIlves
     }else if (track==='Jalg' && hunt==='Hunt-btn'){
        nahtudHunt = "Nähtud hundi jälgi"
         document.getElementById('Edasta').classList.remove("hidden")
         document.getElementById('Kiskjad').classList.add("hidden")
         return nahtudHunt
    }else{
        console.log(event.target.id)
        console.log(document.getElementById(event.target.id))
        var leng = event.target.id.length-4
        var nupp = event.target.id.substring(0,leng)
        console.log("Nupp:" + nupp + ":")
        console.log(document.getElementById(nupp))
        document.getElementById(nupp).classList.remove("hidden")
        document.getElementById('Kiskjad').classList.add("hidden")
    }
}

var edasi
document.querySelectorAll('#Edasi1-btn, #Edasi2-btn, #Edasi3-btn, #Edasi4-btn').forEach(function(item){
    item.onclick = function (event){
        edasi = event.target.id
        document.getElementById('Edasta').classList.remove("hidden")
        document.getElementById('Ilves').classList.add("hidden")
        document.getElementById('Hunt').classList.add("hidden")
        document.getElementById('Karu').classList.add("hidden")
        document.getElementById('Karujalg').classList.add("hidden")
    }
})

var tagasi
document.querySelectorAll('#Tagasi3-btn, #Tagasi4-btn, #Tagasi5-btn, #Tagasi6-btn').forEach(function(item){
    item.onclick = function (event){
        tagasi = event.target.id
        document.getElementById('Kiskjad').classList.remove("hidden")
        document.getElementById('Ilves').classList.add("hidden")
        document.getElementById('Hunt').classList.add("hidden")
        document.getElementById('Karu').classList.add("hidden")
        document.getElementById('Karujalg').classList.add("hidden")
    }
})



var x = document.getElementById("GPS");
function showPosition(position) {
    console.log("position:", position)
    x.innerHTML = "Latitude: " + position.coords.latitude +
    "<br>Longitude: " + position.coords.longitude;
    laiusKraad = position.coords.latitude
    pikkusKraad = position.coords.longitude
}

function getLocation() {
    if (navigator.geolocation) {
       navigator.geolocation.getCurrentPosition(showPosition);
     } else {
       x.innerHTML = "Asukoha määramise funktsioon ei ole sisse lülitatud";
     }
}
