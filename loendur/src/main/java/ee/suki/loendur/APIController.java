package ee.suki.loendur;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.ArrayList;

@RestController
@CrossOrigin
public class APIController {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @PostMapping("/suki")
    void andmed(@RequestBody Andmed vaatlusleht) throws IOException, MessagingException {
        System.out.println("heii");
        System.out.println(vaatlusleht.getHuntide_arv());
        String sqlKask = "INSERT INTO vaatlusleht (id, nimi, telefon, huntide_arv, hundikutsikate_arv, nahtud_hundi_jalg," +
                " ilveste_arv, ilvese_kutsikate_arv, nahtud_ilvese_jalg, karude_arv, karu_paka_cm, karupoegi_kuni_aastased," +
                " karupoegi_yle_aasta, karupoja_paka_cm, nahtud_karu_jalg, karu_jalg_paka_cm, laiuskraad, pikkuskraad, kommentaar) " +
                "VALUES ('" + vaatlusleht.getId() + "', '" + vaatlusleht.getNimi() + "', '" + vaatlusleht.getTelefon() + "', " + vaatlusleht.getHuntide_arv() +
                ", " + vaatlusleht.getHundikutsikate_arv() + ", '" + vaatlusleht.getNahtud_hundi_jalg() + "', " + vaatlusleht.getIlveste_arv() +
                ", " + vaatlusleht.getIlvese_kutsikate_arv() + ", '" + vaatlusleht.getNahtud_ilvese_jalg() +
                "', '" + vaatlusleht.getKarude_arv() + "', '" + vaatlusleht.getKaru_paka_cm() + "', '" + vaatlusleht.getKarupoegi_kuni_aastased() +
                "', '" + vaatlusleht.getKarupoegi_yle_aasta() + "', '" + vaatlusleht.getKarupoja_paka_cm() + "', '" + vaatlusleht.getNahtud_karu_jalg() + "', '" + vaatlusleht.getKaru_jalg_paka_cm() +
                "', '" + vaatlusleht.getLaiuskraad() + "', '" + vaatlusleht.getPikkuskraad() + "', '" + vaatlusleht.getKommentaar() + "')";
        System.out.println(sqlKask);
        jdbcTemplate.execute(sqlKask);
        Email.send("saaja@naidis.ee", "Teavitus", "<p>Tere!</p> <p>Suurkiskjate loendusaandmete andmebaasis on uus sissekanne.</p>");
    }

}
