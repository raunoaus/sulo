package ee.suki.loendur;

public class Andmed {
    private String id;
    private String nimi;
    private String telefon;
    private int huntide_arv;
    private int hundikutsikate_arv;
    private String nahtud_hundi_jalg;
    private int ilveste_arv;
    private int ilvese_kutsikate_arv;
    private String nahtud_ilvese_jalg;
    private int karude_arv;
    private double karu_paka_cm;
    private int karupoegi_kuni_aastased;
    private int karupoegi_yle_aasta;
    private double karupoja_paka_cm;
    private String nahtud_karu_jalg;
    private double karu_jalg_paka_cm;
    private String laiuskraad;
    private String pikkuskraad;
    private String kommentaar;


    public Andmed() {

    }

    public Andmed(String id, String nimi, String telefon, int huntide_arv, int hundikutsikate_arv, String nahtud_hundi_jalg, int ilveste_arv,
                  int ilvese_kutsikate_arv, String nahtud_ilvese_jalg, int karude_arv, double karu_paka_cm, int karupoegi_kuni_aastased, int karupoegi_yle_aasta,
                  double karupoja_paka_cm, String nahtud_karu_jalg, double karu_jalg_paka_cm, String laiuskraad, String pikkuskraad, String kommentaar) {
        this.id = id;
        this.nimi = nimi;
        this.telefon = telefon;
        this.huntide_arv = huntide_arv;
        this.hundikutsikate_arv = hundikutsikate_arv;
        this.nahtud_hundi_jalg = nahtud_hundi_jalg;
        this.ilveste_arv = ilveste_arv;
        this.ilvese_kutsikate_arv = ilvese_kutsikate_arv;
        this.nahtud_ilvese_jalg = nahtud_ilvese_jalg;
        this.karude_arv = karude_arv;
        this.karu_paka_cm = karu_paka_cm;
        this.karupoegi_kuni_aastased = karupoegi_kuni_aastased;
        this.karupoegi_yle_aasta = karupoegi_yle_aasta;
        this.karupoja_paka_cm = karupoja_paka_cm;
        this.nahtud_karu_jalg = nahtud_karu_jalg;
        this.karu_jalg_paka_cm = karu_jalg_paka_cm;
        this.laiuskraad = laiuskraad;
        this.pikkuskraad = pikkuskraad;
        this.kommentaar = kommentaar;
            }

    public String getNimi() {
        return nimi;
    }

    public String getTelefon() {
        return telefon;
    }

    public String getId() { return id; }

    public int getHuntide_arv() {
        return huntide_arv;
    }

    public int getHundikutsikate_arv() { return hundikutsikate_arv; }

    public String getNahtud_hundi_jalg() { return nahtud_hundi_jalg; }

    public int getIlveste_arv() { return ilveste_arv; }

    public int getIlvese_kutsikate_arv() { return ilvese_kutsikate_arv; }

    public String getNahtud_ilvese_jalg() { return nahtud_ilvese_jalg; }

    public int getKarude_arv() { return karude_arv; }

    public double getKaru_paka_cm() { return karu_paka_cm; }

    public int getKarupoegi_kuni_aastased() { return karupoegi_kuni_aastased; }

    public int getKarupoegi_yle_aasta() { return karupoegi_yle_aasta; }

    public double getKarupoja_paka_cm() { return karupoja_paka_cm; }

    public String getNahtud_karu_jalg() { return nahtud_karu_jalg; }

    public double getKaru_jalg_paka_cm() { return karu_jalg_paka_cm; }

    public String getLaiuskraad() {return laiuskraad; }

    public String getPikkuskraad() {return pikkuskraad; }

    public String getKommentaar() {
        return kommentaar;
    }



}
