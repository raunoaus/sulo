package ee.suki.loendur;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;

@SpringBootApplication
public class LoendurApplication implements CommandLineRunner{

	public static void main(String[] args) {
		SpringApplication.run(LoendurApplication.class, args);
	}

	@Autowired
	JdbcTemplate jdbcTemplate;


	@Override
	public void run(String... args) throws Exception {
		System.out.println("Konfigureeri");
		jdbcTemplate.execute("DROP TABLE IF EXISTS vaatlusleht");
		jdbcTemplate.execute("CREATE TABLE vaatlusleht (id TEXT" + ", aeg TIMESTAMPTZ DEFAULT Now(), nimi TEXT, " +
				"telefon TEXT, huntide_arv INT, hundikutsikate_arv INT, nahtud_hundi_jalg TEXT, ilveste_arv INT," +
				"ilvese_kutsikate_arv INT, nahtud_ilvese_jalg TEXT, " +
				"karude_arv INT, karu_paka_cm DOUBLE PRECISION, karupoegi_kuni_aastased INT, karupoegi_yle_aasta INT, " +
				"karupoja_paka_cm DOUBLE PRECISION, nahtud_karu_jalg TEXT, karu_jalg_paka_cm DOUBLE PRECISION, laiuskraad TEXT, pikkuskraad TEXT," +
				" kommentaar TEXT)");

	}

}
